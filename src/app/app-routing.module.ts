import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { authGuard } from './core/guard/auth.guard';
import { adminGuard } from './core/guard/auth-admin.guard';
import { AppLayoutComponent } from './layout/app.layout.component';

const routes: Routes = [
    {
        path: '', component: AppLayoutComponent,
        children: [          
            {
                path: '',
                data: { breadcrumb: 'List Ward' },
                canActivate: [authGuard],
                loadChildren: () => import('./components/apps/list-ward/list-ward.module').then(m => m.ListWardModule)
            },
            { path: 'patient-list', data: { breadcrumb: 'Patien List' }, canActivate: [authGuard],loadChildren: () => import('./components/apps/patient-list/patient-list.module').then(m => m.PatientListModule) },
            { path: 'register', data: { breadcrumb: 'Register' }, canActivate: [authGuard],loadChildren: () => import('./components/apps/register/register.module').then(m => m.RegisterModule) },
            { path: 'waiting-admit', data: { breadcrumb: 'Waiting Admit' },canActivate: [authGuard], loadChildren: () => import('./components/apps/waiting-admit/waiting-admit.module').then(m => m.WaitingAdmitModule) },
            { path: 'nurse-note', data: { breadcrumb: 'Nurse Note' },canActivate: [authGuard], loadChildren: () => import('./components/apps/nurse-note/nurse-note.module').then(m => m.NurseNoteModule) },
            { path: 'change-ward', data: { breadcrumb: 'Change Ward' },canActivate: [authGuard], loadChildren: () => import('./components/apps/change-ward/change-ward.module').then(m => m.ChangeWardModule) },
        ]
    },
    // { path: 'auth', data: { breadcrumb: 'Auth' }, loadChildren: () => import('./demo/components/auth/auth.module').then(m => m.AuthModule) },
    { path: 'login', data: { breadcrumb: 'Login' }, loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule) },
    { path: 'landing', loadChildren: () => import('./components/landing/landing.module').then(m => m.LandingModule) },
    { path: 'notfound', loadChildren: () => import('./components/notfound/notfound.module').then(m => m.NotfoundModule) },
    { path: '**', redirectTo: '/notfound' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
