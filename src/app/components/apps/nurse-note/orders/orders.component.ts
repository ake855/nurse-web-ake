

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, MenuItem, MessageService } from 'primeng/api';
import { DoctorOrderService } from './doctor-order-service';


import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  providers: [MessageService],

})
export class OrdersComponent implements OnInit {
  parentData: any;
  // @Input() parentData: any;
  dataLoaded: boolean = false;
  // dataLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  // progress note data
  progressValueS?: string;
  progressValueO?: string;
  progressValueA?: string;
  progressValueP?: string;
  onedayValue?: string;
  continueValue?: string;

  // doctor order data
  doctorOrderData: any;
  listDoctorOrder: any;

  userData: any;

  // is new order
  isNewOrder: boolean = true;

  progressTags = ['progressValue'];
  onedayTags = ['onedayValue'];
  continueTags = ['onedayValue'];

  inputVisible = false;
  inputValue?: string = '';

  patientList: any;
  orderList: any;
  drugList: any;

  hn: any;
  an: any;

  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;

  admit_id: any;
  doctor_order_date: any;
  doctor_order_time: any;
  doctor_order_by: any;
  is_confirm: any;

  subjective: any;
  objective: any;
  assertment: any;
  plan: any;
  note: any;

  order_type_id: any;
  item_type_id: any;
  item_id: any;
  item_name: any;
  medicine_usage_code: any;
  medicine_usage_extra: any;
  quantity: any;

  food_id: any;
  start_date: any;
  start_time: any;
  end_date: any;
  end_time: any;

  is_vital_sign: any;

  progress_note: any;
  progress_note_subjective: any[] = [];
  progress_note_objective: any[] = [];
  progress_note_assertment: any[] = [];
  progress_note_plan: any[] = [];
  oneDay: any[] = [];
  continue: any[] = [];
  orders: any[] = [];
  ordersOneday: any[] = [];
  continues: any[] = [];

  medicine: any[] = [];
  speedDialitems: MenuItem[] = [];
  progress_note_id: any;

  drugname: any;
  indeterminate = true;

  queryParamsData: any;
  value?: string;
  inputValue1?: string;
  jsonData: any = {

  }


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private doctorOrderService: DoctorOrderService,
   
  ) {
    let _data = sessionStorage.getItem('doctor_order');
    // sessionStorage.removeItem('doctor_order');
    // console.log(_data);

    let _user = sessionStorage.getItem('userData');
    if (_user != "undefined" && _user != null) {
      let user = JSON.parse(_user!);
      this.userData = user;
    } else {
      this.userData = null;
    }

    let jsonString: any =
      this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    // console.log(this.queryParamsData);


  }

  async ngOnInit(): Promise<void> {
    // console.log('is init doctor order');
    const userDataString: any = sessionStorage.getItem('userData');
    const userJson = JSON.parse(userDataString);
    // console.log(userJson);

    this.getPatient();
    this.buttonSpeedDial();
    //     const dk = await this.getUserById("b83e8ae9-c0c7-48c7-9a48-c6d3901a546f");
    // console.log(dk);
  }

  ngAferViewInit() {
    // this.getPatient();
  }


  async getPatient() {
    // console.log('getPatient');
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      //  console.log(this.queryParamsData);
      let admitId = this.queryParamsData.id;

      const response = await this.doctorOrderService.getPatientInfo(admitId);

      const data: any = response.data;

      this.patientList = await data.data;
      //console.log(this.patientList);
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title = this.patientList.patient.title;
      this.fname = this.patientList.patient.fname;
      this.lname = this.patientList.patient.lname;
      this.gender = this.patientList.patient.gender;
      this.age = this.patientList.patient.age;
      this.address = this.patientList.address;
      this.phone = this.patientList.phone;

      this.total = data.total || 1;
      // this.messageService.add({
      //   severity: 'success',
      //   summary: 'กระบวนการสำเร็จ #',
      //   detail: '.....',
      // });

      let docterOrder = await this.doctorOrderService.getDoctorOrderById(this.queryParamsData.id);
      // console.log(docterOrder);
      this.dataLoaded = true;

      if (docterOrder.data.data.length > 0) {
        let sortData = _.orderBy(docterOrder.data.data, ["doctor_order_date", "doctor_order_time"], ['desc', 'desc']);
        // console.log(sortData);
        this.listDoctorOrder = sortData;
        this.isNewOrder = sortData[0].is_confirm;
        // console.log(this.listDoctorOrder);
        // console.log(this.isNewOrder);
        var date2 = DateTime.now();
        var date3 = DateTime.now();
        const date5 = date2.toFormat('yyyy-MM-dd');
        const time1 = date3.toFormat('HH:mm:ss');
        let doctor_order = [{
          "id": sortData[0].id,
          "admit_id": sortData[0].admit_id,
          "doctor_order_date": sortData[0].doctor_order_date,
          "doctor_order_time": sortData[0].doctor_order_time,
          "doctor_order_by": sortData[0].doctor_order_by,
          "is_confirm": sortData[0].is_confirm,
          "is_active": true,
          "create_date": sortData[0].doctor_order_date,
          "create_by": null,
          "modify_date": date5,
          "modify_by": this.userData.id || null,
          "status": "pending",
          "confirm_by": null,
          "confirm_date": null,
          "confirm_time": null
        }]

        // console.log(doctor_order);
        this.doctorOrderData = doctor_order;
        this.parentData = { 'isNewOrder': this.isNewOrder, 'doctor_order': doctor_order };
        let data = { 'isNewOrder': this.isNewOrder, 'doctor_order': doctor_order };

        sessionStorage.setItem('doctor_order', JSON.stringify(data));


        this.dataLoaded = true;


      } else {
        this.isNewOrder = true;
        // console.log(this.isNewOrder);
        let data = { 'isNewOrder': true, 'doctor_order': [] };


        sessionStorage.setItem('doctor_order', JSON.stringify(data));


        this.dataLoaded = true;

      }



    } catch (error: any) {
      console.log(error);

      this.messageService.add({
        severity: 'dager',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }
  async confirmOrder(items: any) {


    // console.log(items);
    const currentDate = DateTime.local().toFormat('yyyy-MM-dd');
    const currentTime = DateTime.local().toFormat('HH:mm:ss');

    const userDataString: any = sessionStorage.getItem('userData');
    const userData = JSON.parse(userDataString);
    items.confirm_date = currentDate;
    items.confirm_time = currentTime;
    items.confirm_by = userData.user_id;
    items.is_confirm = true;
    // console.log(items);
    delete items.order;
    delete items.progress_note;
    // delete items.id;

    const data = { "doctor_order": [items] };
    // console.log(data);

    try {
      const response = await this.doctorOrderService.saveDoctorOrder(
        data
      );
      // console.log(response);
      if (response.data.ok) {
        this.getPatient();
        this.parentData = { 'isNewOrder': true, 'doctor_order': [] };

        sessionStorage.setItem(
          'doctor_order',
          JSON.stringify(this.parentData)
        );

        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'บันทึกสำเร็จ' });
      } else {
        this.messageService.add({ severity: 'danger', summary: 'Error', detail: 'ไม่สามารถบันทึกได้' });

      }

    } catch (error) {
      console.log('saveDoctorOrder:' + error);
    }
  }

  back() {
    sessionStorage.removeItem('doctor_order');
    this.router.navigate(['/list-patient']);
  }

  buttonSpeedDial() {
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote()
        },
        tooltipOptions: {
          tooltipLabel: 'Admission Note',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'bottom'
        },
      }
    ];
  }

  navigatePatientInfo() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

  }

  navigateAdmisstionNote() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }

  navigateEkg() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });

  }

  navigateConsult() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

  }
  navigateLab() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }
  async getUserById(user_id: any): Promise<string | undefined> {
    // console.log('getUserById');
    try {
      let data = await this.doctorOrderService.getUserInfo(user_id);
      // console.log(data);
      let userName =
        data.data.data[0].profile.title +
        '' +
        data.data.data[0].profile.fname +
        ' ' +
        data.data.data[0].profile.lname;
      return userName;
    } catch (error) {
      console.error(error);
      throw new Error('Error occurred while fetching user information');
    }
  }


  async removeProgressNote(type: string) {
    if (type == 'S') {

      await this.addProgressNote('S');
    } else if (type == 'O') {

      await this.addProgressNote('O');
    } else if (type == 'A') {

      await this.addProgressNote('A');
    } else if (type == 'P') {

      await this.addProgressNote('P');
    } else {

      await this.addProgressNote('N');
    }
  }


  async addProgressNote(type: string) {


    let progress_note: any; // set progress note

    // select type of progress note
    if (type == 'S') {
      progress_note = {
        subjective: [''],
      };

    } else if (type == 'O') {

      progress_note = {
        objective: [''],
      };

    } else if (type == 'A') {

      progress_note = {
        assertment: [''],
      };

    } else if (type == 'P') {


      progress_note = {
        plan: [''],
      };

    } else {

      progress_note = {
        note: [''],
      };

    }

    try {
      const response = await this.saveProgressNote(progress_note); // save progress note
      // console.log(response);
    } catch (error) {
      console.log('save progress note:' + error);
    }

  }

  async saveProgressNote(datas: any) {
    // set data to save

    let data = {
      doctor_order: this.doctorOrderData,
      progress_note: [datas],
    };
    // console.log('progress_note:', data);

    try {
      const response = await this.doctorOrderService.saveDoctorOrder(
        data
      );
      // console.log(response);
    } catch (error) {
      console.log('saveDoctorOrder:' + error);
    }
  }

  async removeOrder(id: any) {
    try {
        let rs = await this.doctorOrderService.removeOrders(id);
        // console.log('response:', rs);
      
    } catch (error) {
        console.log('remove Doctor Order:' + error);
    }
}

}

